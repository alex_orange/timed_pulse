import time
import sys

import numpy

import uhd

f = 3651 * 1000 * 1000
lo_offset = 50 * 1000 * 1000
rate = 20 * 1000 * 1000
rx_gain = 31.5
antenna = 'TX/RX'

mboard_name = 'X310'

try:
    device = uhd.usrp.MultiUSRP()
except Exception as e:
    print(e)
    sys.exit(1)

if device.get_mboard_name() != mboard_name:
    print(f"Expected radio type: {mboard_name}, got radio type "
            f"{device.get_mboard_name()}")
    sys.exit(2)

device.set_clock_source('external')
device.set_time_source('external')

time.sleep(2)

device.set_time_next_pps(uhd.libpyuhd.types.time_spec(0))
time.sleep(2)

tune_req = uhd.libpyuhd.types.tune_request(f, lo_offset)
device.set_rx_freq(tune_req, 0)

device.set_rx_rate(rate, 0)

device.set_rx_gain(rx_gain, 0)

device.set_rx_antenna(antenna, 0)

device.set_rx_dc_offset(False)



stream_args = uhd.libpyuhd.usrp.stream_args("fc32", "sc16")
stream_args.channels = [0]

rx_stream = device.get_rx_stream(stream_args)



n_rx = 20 * 1000 * 1000

rx_buff = numpy.zeros((1, n_rx), dtype=numpy.complex64)



metadata = uhd.libpyuhd.types.rx_metadata()



stream_cmd = uhd.libpyuhd.types.stream_cmd(
        uhd.libpyuhd.types.stream_mode.start_cont)
stream_cmd.num_samps = n_rx
stream_cmd.stream_now = False
stream_cmd.time_spec = uhd.libpyuhd.types.time_spec(5)

rx_stream.issue_stream_cmd(stream_cmd)

print(device.get_time_now().get_real_secs())


n = 10 * 1000
rx_tmp_buff = numpy.zeros((1, n), dtype=numpy.complex64)

total_count = 0

while total_count < n_rx:
    new_samps = rx_stream.recv(rx_tmp_buff, metadata)

    if new_samps > n_rx - total_count:
        new_samps = n_rx - total_count
    a = total_count + new_samps

    rx_buff[:, total_count:a] = rx_tmp_buff[:, 0:new_samps]

    total_count += new_samps


stream_cmd = uhd.libpyuhd.types.stream_cmd(
        uhd.libpyuhd.types.stream_mode.stop_cont)
stream_cmd.num_samps = n_rx
stream_cmd.stream_now = False
stream_cmd.time_spec = uhd.libpyuhd.types.time_spec(5)

rx_stream.issue_stream_cmd(stream_cmd)


print(total_count)

n = 400

energies = []

for i in range(n_rx // n):
    energies.append(abs(sum(rx_buff[0, i*n:(i+1)*n])))

nn = 100
m = 90
trig = 0.06

for i in range(len(energies)-nn):
    count = 0
    for j in range(nn):
        if energies[i+j] > trig:
            count += 1

    if count >= m:
        print(i, i/len(energies))
        break

while True:
    stream_cmd = uhd.libpyuhd.types.stream_cmd(
            uhd.libpyuhd.types.stream_mode.start_cont)
    stream_cmd.num_samps = n_rx
    stream_cmd.stream_now = False
    stream_cmd.time_spec = uhd.libpyuhd.types.time_spec(
            (device.get_time_now().get_real_secs()+0.2)//1 + 1)

    try:
        rx_stream.issue_stream_cmd(stream_cmd)

        print(stream_cmd.time_spec.get_real_secs(),
                device.get_time_now().get_real_secs())


        n = 10 * 1000
        rx_tmp_buff = numpy.zeros((1, n), dtype=numpy.complex64)

        total_count = 0

        while total_count < n_rx:
            new_samps = rx_stream.recv(rx_tmp_buff, metadata)

            if new_samps > n_rx - total_count:
                new_samps = n_rx - total_count
            a = total_count + new_samps

            rx_buff[:, total_count:a] = rx_tmp_buff[:, 0:new_samps]

            total_count += new_samps
    finally:
        stream_cmd = uhd.libpyuhd.types.stream_cmd(
                uhd.libpyuhd.types.stream_mode.stop_cont)
        stream_cmd.num_samps = n_rx
        stream_cmd.stream_now = False
        stream_cmd.time_spec = uhd.libpyuhd.types.time_spec(5)

        rx_stream.issue_stream_cmd(stream_cmd)


    n = 400

    energies = []

    for i in range(n_rx // n):
        energies.append(abs(sum(rx_buff[0, i*n:(i+1)*n])))

    nn = 50
    m = 40
    trig = 0.06

    got = True

    for i in range(-nn, 2*len(energies)):
        count = 0
        for j in range(nn):
            k = i+j
            k %= len(energies)
            if energies[k] > trig:
                count += 1

        if count >= m:
            if not got:
                print(i, i/len(energies))
                break

        got = count >= m
