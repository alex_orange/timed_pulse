import uhd
import time

mboard_name = 'X310'

try:
    device = uhd.usrp.MultiUSRP()
except e:
    print('Error getting sdr')

if device.get_mboard_name() != mboard_name:
    print(f'Expected radio: {mboard_name}, got radio: {device.get_mboard_name()}')
    sys.exit(1)

device.set_clock_source('external')
device.set_time_source('external')

device.set_time_next_pps(uhd.libpyuhd.types.time_spec(0))

time.sleep(2)

while True:
    print(device.get_time_last_pps().get_real_secs())
    time.sleep(1)
