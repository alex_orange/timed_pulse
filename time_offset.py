import uhd
import time

mboard_name = 'X310'

try:
    device = uhd.usrp.MultiUSRP()
except Exception as e:
    print('Error getting sdr')
    print(e)

if device.get_mboard_name() != mboard_name:
    print(f'Expected radio type: {mboard_name}, got radio type: '
          f'{device.get_mboard_name()}')
    sys.exit(1)

device.set_clock_source('external')
device.set_time_source('external')

device.set_time_next_pps(uhd.libpyuhd.types.time_spec(0))

time.sleep(2)

i = 0

while True:
    start = time.time()
    sdr_time = device.get_time_now().get_real_secs()
    sdr_time_query_elapsed = time.time()
    mod_node_time = start % 1
    mod_sdr_time = sdr_time % 1

    if i % 20 == 0:
        print("time spent querying SDR time, node time, sdr time, offset")

    i += 1

    query_time = sdr_time_query_elapsed - start
    offset = mod_node_time - mod_sdr_time

    if offset < 0:
        offset += 1
    print(f"{1000*query_time:6.3f}  {mod_node_time:.3f}  {mod_sdr_time:.3f}  "
          f"{offset:.6f}")
    time.sleep(1)
