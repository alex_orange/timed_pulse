import time
import sys

import numpy

import uhd

f = 3651 * 1000 * 1000
lo_offset = -50 * 1000 * 1000
rate = 20 * 1000 * 1000
tx_gain = 31.5
antenna = 'TX/RX'

mboard_name = 'X310'

try:
    device = uhd.usrp.MultiUSRP()
except Exception as e:
    print(e)
    sys.exit(1)

if device.get_mboard_name() != mboard_name:
    print(f"Expected radio type: {mboard_name}, got radio type "
            f"{device.get_mboard_name()}")
    sys.exit(2)

device.set_clock_source('external')
device.set_time_source('external')

time.sleep(2)

device.set_time_next_pps(uhd.libpyuhd.types.time_spec(0))
time.sleep(2)

tune_req = uhd.libpyuhd.types.tune_request(f, lo_offset)
device.set_tx_freq(tune_req, 0)

device.set_tx_rate(rate, 0)

device.set_tx_gain(tx_gain, 0)

device.set_tx_antenna(antenna, 0)



stream_args = uhd.libpyuhd.usrp.stream_args("fc32", "sc16")
stream_args.channels = [0]

tx_stream = device.get_tx_stream(stream_args)



n_tx = 20000

tx_buff = numpy.zeros((1, n_tx), dtype=numpy.complex64)
for i in range(n_tx):
    tx_buff[0, i] = 1+1j

print(tx_buff[0])


metadata = uhd.libpyuhd.types.tx_metadata()

metadata.start_of_burst = True
metadata.end_of_burst = True
metadata.has_time_spec = True
metadata.time_spec = uhd.libpyuhd.types.time_spec(
        device.get_time_now().get_real_secs() // 1 + 1)

print(device.get_time_now().get_real_secs())

last_time = device.get_time_now().get_real_secs() - 1

while True:
    dev_time = device.get_time_now().get_real_secs()

    if dev_time // 1 < last_time:
        time.sleep(0.1)

        continue

    if dev_time % 1 > 0.7:
        last_time = device.get_time_now().get_real_secs() // 1 + 1
        print(f"Sending at time {last_time}")
        metadata.time_spec = uhd.libpyuhd.types.time_spec(last_time)

        tx_stream.send(tx_buff, metadata)

        time.sleep(0.5)
    else:
        time.sleep(0.1)
